/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * PCB Cooker                                                  *
 *   EEPROM memory:                                            *
 *    0x0000       - Default profile                           *
 *    0x0001       - Default unit (0xFF -F / 0x00 -C)          *
 *    0x0002-0x0003 - Device's max temp                        *
 *    0x0100-0x010B - Profile Name 1                           *
 *    0x0110-0x011B - Profile Name 2                           *
 *    0x0120-0x012B - Profile Name 3                           *
 *    0x0130-0x013B - Profile Name 4                           *
 *    0x0140-0x014B - Profile Name 5                           *
 *    0x0150-0x015B - Profile Name 6                           *
 *    0x0160-0x016B - Profile Name 7                           *
 *    0x0170-0x017B - Profile Name 8                           *
 *    0x0180-0x018B - Profile Name 9                           *
 *    0x0190-0x019B - Profile Name 10                          *
 *    0x0200-0x0214 - Profile 1 Heat profile                   *
 *    0x0200-0x0214 - Profile 2 Heat profile                   *
 *    0x0200-0x0214 - Profile 3 Heat profile                   *
 *    0x0200-0x0214 - Profile 4 Heat profile                   *
 *    0x0200-0x0214 - Profile 5 Heat profile                   *
 *    0x0200-0x0214 - Profile 6 Heat profile                   *
 *    0x0200-0x0214 - Profile 7 Heat profile                   *
 *    0x0200-0x0214 - Profile 8 Heat profile                   *
 *    0x0200-0x0214 - Profile 9 Heat profile                   *
 *    0x0200-0x0214 - Profile 10 Heat profile                  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
//max temp 450F/230C
#include <EEPROM.h>
#include <LiquidCrystal.h>
LiquidCrystal lcd(8,9,4,5,6,7); 

#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5

#define keyRIGHT  30
#define keyUP     150
#define keyDOWN   360
#define keyLEFT   535
#define keySELECT 760
#define keyNONE   770

#define mnuMAIN 0
#define mnuPROF 1
#define mnuCOOK 2
#define mnuPPRO 3 //pick profile
#define mnuEPRO 4 //edit profile

byte menuLoc = mnuMAIN;
byte menuLocOld = mnuMAIN;
byte keyValue = 0;
char btnPush = btnNONE;
byte menuPage = 1;
byte menuPageOld = 0;

void Version() {
 lcd.clear();
 lcd.print("PCB Cooker v0.1");
 lcd.setCursor(0, 1);
 lcd.print("   build 0040    ");
}
 
void setup() {
 lcd.begin(16,2);
 Boot();
 MainMenu();
}

void loop() {
 btnPush = ReadKey();
 WaitKeyRelease();
 if (menuLoc == mnuMAIN) {
  if (btnPush == btnSELECT) { 
   switch (menuPage) {
    case 1:
     menuLocOld = menuLoc;
     menuLoc = mnuCOOK;
     menuPage = 1;
     menuPageOld = 1;
     break;
    case 2:
     menuLocOld = menuLoc;
     menuLoc = mnuPROF;
     menuPage = 1;
     menuPageOld = 1;
     break;
    case 3:
     MenuWeb();
     break;
    case 4:
     MenuVer();
     break;
   }
  }
 } else if (menuLoc == mnuPROF) {
  if (btnPush == btnSELECT) { 
   switch (menuPage) {
    case 1:
     menuLocOld = menuLoc;
     menuLoc = mnuPPRO;
     menuPage = 1;
     menuPageOld = 1;
     break;
    case 2:
     menuLocOld = menuLoc;
     menuLoc = mnuEPRO;
     menuPage = 1;
     menuPageOld = 1;
     break;
    case 3:
     menuLocOld = menuLoc;
     menuLoc = mnuMAIN;
     menuPage = 1;
     menuPageOld = 1;
     break; 
   }
  } 
 } else if (menuLoc == mnuCOOK) {  
  if (btnPush == btnSELECT) { 
   switch (menuPage) {
    case 1:
     MenuVer();
     break;
    case 2:
     MenuVer();
     break;
    case 3:
     menuLocOld = menuLoc;
     menuLoc = mnuMAIN;
     menuPage = 1;
     menuPageOld = 1;
     break; 
   }
  } 
 } else if (menuLoc == mnuPPRO) { 
  if (btnPush == btnSELECT) { 
   switch (menuPage) {
    case 1:
     SetActiveProfile(1);
     break;
    case 2:
     SetActiveProfile(2);
     break;
    case 3:
     SetActiveProfile(3);
     break;
    case 4:
     SetActiveProfile(4);
     break;
    case 5:
     SetActiveProfile(5);
     break;
    case 6:
     SetActiveProfile(6);
     break;
    case 7:
     SetActiveProfile(7);
     break;
    case 8:
     SetActiveProfile(8);
     break;
    case 9:
     SetActiveProfile(9);
     break;
    case 10:
     SetActiveProfile(10);
     break;
    case 11:
     break; 
   }
   if (menuPage != 11) { ProfSelected(); }
   menuLocOld = menuLoc;
   menuLoc = mnuPROF;
   menuPage = 1;
   menuPageOld = 1;
  }   
 } else if (menuLoc == mnuEPRO) {
  if (btnPush == btnSELECT) { 
   switch (menuPage) {
    case 1:
     MenuVer();
     break;
    case 2:
     MenuVer();
     break;
    case 3:
     MenuVer();
     break;
    case 4:
     MenuVer();
     break;
    case 5:
     MenuVer();
     break;
    case 6:
     MenuVer();
     break;
    case 7:
     MenuVer();
     break;
    case 8:
     MenuVer();
     break;
    case 9:
     MenuVer();
     break;
    case 10:
     MenuVer();
     break;
    case 11:
     menuLocOld = menuLoc;
     menuLoc = mnuPROF;
     menuPage = 1;
     menuPageOld = 1;
     break; 
   }
  }    
 }
 WaitKeyRelease();
 Display();
 delay(10);
}

void Boot() {
 Website();
 delay(2000);
 Version();
 delay(3000);
}
void Website() {
 lcd.clear();
 lcd.print("Wicked Software");
 lcd.setCursor(0, 1);
 lcd.print("wck.bz/PCBCook");
}
void MenuVer() {  
 Version();
 WaitKeyRelease();
 while (ReadKey() == btnNONE) { }
 menuLocOld = menuLoc;
 menuLoc = mnuMAIN;
 menuPage = 1;
 menuPageOld = 0;
}
void MenuWeb() {  
 Website();   
 WaitKeyRelease();
 while (ReadKey() == btnNONE) { }
 menuLocOld = menuLoc;
 menuLoc = mnuMAIN;
 menuPage = 1;
 menuPageOld = 0;
}
 
void Display() {
 WaitKeyRelease();
 if (btnPush == btnUP) {
  menuPage--;
  if (menuPage == 0) {
   if (menuLoc == mnuMAIN) {
    menuPage = 4;
   } else if (menuLoc == mnuPROF) {
    menuPage = 3; 
   } else if (menuLoc == mnuCOOK) { 
    menuPage = 3; 
   } else if (menuLoc == mnuPPRO) { 
    menuPage = 11; 
   } else if (menuLoc == mnuEPRO) { 
    menuPage = 11; 
   }
  }
 } else if (btnPush == btnDOWN) {
  menuPage++;
  if (menuLoc == mnuMAIN) {
   if (menuPage > 4) { menuPage = 1; }
  } else if (menuLoc == mnuPROF) {
   if (menuPage > 3) { menuPage = 1; } 
  } else if (menuLoc == mnuCOOK) {
   if (menuPage > 3) { menuPage = 1; } 
  } else if (menuLoc == mnuPPRO) {
   if (menuPage > 11) { menuPage = 1; } 
  } else if (menuLoc == mnuEPRO) {
   if (menuPage > 11) { menuPage = 1; } 
  }
 }
 if ((menuLoc != menuLocOld) || (menuPage != menuPageOld)) {
  if (menuLoc != menuLocOld) { menuLocOld = menuLoc; }
  if (menuPage != menuPageOld) { menuPageOld = menuPage; }
  if (menuLoc == mnuMAIN) {
   MainMenu();
  } else if (menuLoc == mnuPROF) {
   ProfileMenu();
  } else if (menuLoc == mnuCOOK) {
   CookMenu();
  } else if (menuLoc == mnuPPRO) {
   PickMenu();
  } else if (menuLoc == mnuEPRO) {
   EditMenu();
  }
 }
}

char ReadKey() {
 keyValue = analogRead(0);
 if (keyValue > keyNONE) return btnNONE; // 1st opt for speed since it will be the most likely
 if (keyValue < keyRIGHT)  return btnRIGHT;  
 if (keyValue < keyUP) return btnUP; 
 if (keyValue < keyDOWN) return btnDOWN; 
 if (keyValue < keyLEFT) return btnLEFT; 
 if (keyValue < keySELECT) return btnSELECT; 
}
void WaitKeyRelease() {
 while(analogRead(0) < keyNONE) {}
}

void MainMenu() {
 lcd.clear();
 lcd.setCursor(0,0);
 switch (menuPage) {
  case 1:
   lcd.print("1. Cooking Menu");
   ShowActiveProfile();
   break;
  case 2:
   lcd.print("2. Profile Menu");
   break;
  case 3:
   lcd.print("3. Website");
   break;
  case 4:
   lcd.print("4. Version");
   break;   
 }
}

void ProfileMenu() {
 lcd.clear();
 lcd.setCursor(0,0);
 switch (menuPage) {
  case 1:
   lcd.print("1. Pick profile");
   ShowActiveProfile();
   break;
  case 2:
   lcd.print("2. Edit profile");
   break; 
  case 3:
   lcd.print("3. Main Menu");
   break;      
 }
 loop();
}

void CookMenu() {
 lcd.clear();
 lcd.setCursor(0,0);
 switch (menuPage) {
  case 1:
   lcd.print("1. Cook");
   ShowActiveProfile();
   break;
  case 2:
   lcd.print("2. Cook");
   lcd.setCursor(0,1);
   lcd.print("   Manually");
   break; 
  case 3:
   lcd.print("3. Main Menu");
   break;      
 }
}

void PickMenu() {
 lcd.clear();
 lcd.setCursor(0,0);
 switch (menuPage) {
  case 1:
   lcd.print("Pick profile 1:");
   lcd.setCursor(2,1);
   ShowName(0x01);
   break;
  case 2:
   lcd.print("Pick profile 2:");
   lcd.setCursor(2,1);
   ShowName(0x02);
   break;
  case 3:
   lcd.print("Pick profile 3:");
   lcd.setCursor(2,1);
   ShowName(0x03);
   break;
  case 4:
   lcd.print("Pick profile 4:");
   lcd.setCursor(2,1);
   ShowName(0x04);
   break;
  case 5:
   lcd.print("Pick profile 5:");
   lcd.setCursor(2,1);
   ShowName(0x05);
   break;
  case 6:
   lcd.print("Pick profile 6:");
   lcd.setCursor(2,1);
   ShowName(0x06);
   break;
  case 7:
   lcd.print("Pick profile 7:");
   lcd.setCursor(2,1);
   ShowName(0x07);
   break;
  case 8:
   lcd.print("Pick profile 8:");
   lcd.setCursor(2,1);
   ShowName(0x08);
   break;
  case 9:
   lcd.print("Pick profile 9:");
   lcd.setCursor(2,1);
   ShowName(0x09);
   break;
  case 10:
   lcd.print("Pick profile 10:");
   lcd.setCursor(2,1);
   ShowName(0x0A);
   break;
  case 11:
   lcd.print("    Back  to    ");
   lcd.setCursor(0,1);
   lcd.print("  Profile Menu  ");
   break;      
 } 
}

void EditMenu() {
 lcd.clear();
 lcd.setCursor(0,0);
 switch (menuPage) {
  case 1:
   lcd.print("Edit profile 1:");
   lcd.setCursor(2,1);
   ShowName(0x01);
   break;
  case 2:
   lcd.print("Edit profile 2:");
   lcd.setCursor(2,1);
   ShowName(0x02);
   break;
  case 3:
   lcd.print("Edit profile 3:");
   lcd.setCursor(2,1);
   ShowName(0x03);
   break;
  case 4:
   lcd.print("Edit profile 4:");
   lcd.setCursor(2,1);
   ShowName(0x04);
   break;
  case 5:
   lcd.print("Edit profile 5:");
   lcd.setCursor(2,1);
   ShowName(0x05);
   break;
  case 6:
   lcd.print("Edit profile 6:");
   lcd.setCursor(2,1);
   ShowName(0x06);
   break;
  case 7:
   lcd.print("Edit profile 7:");
   lcd.setCursor(2,1);
   ShowName(0x07);
   break;
  case 8:
   lcd.print("Edit profile 8:");
   lcd.setCursor(2,1);
   ShowName(0x08);
   break;
  case 9:
   lcd.print("Edit profile 9:");
   lcd.setCursor(2,1);
   ShowName(0x09);
   break;
  case 10:
   lcd.print("Edit profile 10:");
   lcd.setCursor(2,1);
   ShowName(0x0A);
   break;
  case 11:
   lcd.print("    Back  to    ");
   lcd.setCursor(0,1);
   lcd.print("  Profile Menu  ");
   break;      
 } 
}

void SetActiveProfile(int pid) {
 if ((pid > 0) && (pid < 11)) {
  EEPROM.update(0, pid);
 } else {
  EEPROM.put(0, 0);
 }
}
void ShowActiveProfile() {
 byte hex = EEPROM.read(0);
 lcd.setCursor(0,1);
 lcd.write("( ");
 ShowName(hex);
 lcd.write(" )");
}

void ShowName(byte hex) {
 byte name;
 if (hex == 0x1) {
  if (CheckName(1)) {
   name = ReadName(1); 
   lcd.write(name);
  } else {
   lcd.print("Profile 1   "); 
  }   
 } else if (hex == 0x2) {
  if (CheckName(2)) { 
   name = ReadName(2); 
   lcd.write(name);
  } else {
   lcd.print("Profile 2   "); 
  }    
 } else if (hex == 0x3) {
  if (CheckName(3)) { 
   name = ReadName(3); 
   lcd.write(name); 
  } else {
   lcd.print("Profile 3   "); 
  } 
 } else if (hex == 0x4) {
  if (CheckName(4)) { 
   name = ReadName(4); 
   lcd.write(name);
  } else {
   lcd.print("Profile 4   "); 
  }      
 } else if (hex == 0x5) {
  if (CheckName(5)) { 
   name = ReadName(5); 
   lcd.write(name);
  } else {
   lcd.print("Profile 5   "); 
  }        
 } else if (hex == 0x6) {
  if (CheckName(6)) { 
   name = ReadName(6); 
   lcd.write(name); 
  } else {
   lcd.print("Profile 6   "); 
  }    
 } else if (hex == 0x7) {
  if (CheckName(7)) { 
   name = ReadName(7); 
   lcd.write(name);
  } else {
   lcd.print("Profile 7   "); 
  }      
 } else if (hex == 0x8) {
  if (CheckName(8)) { 
   name = ReadName(8); 
   lcd.write(name);
  } else {
   lcd.print("Profile 8   "); 
  }        
 } else if (hex == 0x9) {
  if (CheckName(9)) { 
   name = ReadName(9); 
   lcd.write(name);
  } else {
   lcd.print("Profile 9   "); 
  }       
 } else if (hex == 0xA) {
  if (CheckName(10)) { 
   name = ReadName(10); 
   lcd.write(name);
  } else {
   lcd.print("Profile 10  "); 
  }      
 } else {
  lcd.print("            ");  
 } 
}

bool CheckName(int nid) {
 byte tmp;
 if (nid == 1) {
  tmp = EEPROM.read(0x100); 
  if (tmp != 0xFF) {
   return true;
  } else { return false; }
 } else if (nid == 2) {
  tmp = EEPROM.read(0x110); 
  if (tmp != 0xFF) {
   return true;
  } else { return false; }  
 } else if (nid == 3) {
  tmp = EEPROM.read(0x120); 
  if (tmp != 0xFF) {
   return true;
  } else { return false; }  
 } else if (nid == 4) {
  tmp = EEPROM.read(0x130); 
  if (tmp != 0xFF) {
   return true;
  } else { return false; }  
 } else if (nid == 5) {
  tmp = EEPROM.read(0x140); 
  if (tmp != 0xFF) {
   return true;
  } else { return false; }  
 } else if (nid == 6) {
  tmp = EEPROM.read(0x150); 
  if (tmp != 0xFF) {
   return true;
  } else { return false; }  
 } else if (nid == 7) {
  tmp = EEPROM.read(0x160); 
  if (tmp != 0xFF) {
   return true;
  } else { return false; }  
 } else if (nid == 8) {
  tmp = EEPROM.read(0x170); 
  if (tmp != 0xFF) {
   return true;
  } else { return false; }  
 } else if (nid == 9) {
  tmp = EEPROM.read(0x180); 
  if (tmp != 0xFF) {
   return true;
  } else { return false; }  
 } else if (nid == 10) {
  tmp = EEPROM.read(0x190); 
  if (tmp != 0xFF) {
   return true;
  } else { return false; }  
 }
}

byte ReadName(int nid) {
 byte tmp;
 if (nid == 1) {
  tmp = ( EEPROM.read(0x100), EEPROM.read(0x101), EEPROM.read(0x102), EEPROM.read(0x103), 
          EEPROM.read(0x104), EEPROM.read(0x105), EEPROM.read(0x106), EEPROM.read(0x107), 
          EEPROM.read(0x108), EEPROM.read(0x109), EEPROM.read(0x10A), EEPROM.read(0x10B) );
 } else if (nid == 2) {
  tmp = ( EEPROM.read(0x110), EEPROM.read(0x111), EEPROM.read(0x112), EEPROM.read(0x113), 
          EEPROM.read(0x114), EEPROM.read(0x115), EEPROM.read(0x116), EEPROM.read(0x117), 
          EEPROM.read(0x118), EEPROM.read(0x119), EEPROM.read(0x11A), EEPROM.read(0x11B) );  
 } else if (nid == 3) {
  tmp = ( EEPROM.read(0x120), EEPROM.read(0x121), EEPROM.read(0x122), EEPROM.read(0x123), 
          EEPROM.read(0x124), EEPROM.read(0x125), EEPROM.read(0x126), EEPROM.read(0x127), 
          EEPROM.read(0x128), EEPROM.read(0x129), EEPROM.read(0x12A), EEPROM.read(0x12B) );
 } else if (nid == 4) {
  tmp = ( EEPROM.read(0x130), EEPROM.read(0x131), EEPROM.read(0x132), EEPROM.read(0x133), 
          EEPROM.read(0x134), EEPROM.read(0x135), EEPROM.read(0x136), EEPROM.read(0x137), 
          EEPROM.read(0x138), EEPROM.read(0x139), EEPROM.read(0x13A), EEPROM.read(0x13B) );  
 } else if (nid == 5) {
  tmp = ( EEPROM.read(0x140), EEPROM.read(0x141), EEPROM.read(0x142), EEPROM.read(0x143), 
          EEPROM.read(0x144), EEPROM.read(0x145), EEPROM.read(0x146), EEPROM.read(0x147), 
          EEPROM.read(0x148), EEPROM.read(0x149), EEPROM.read(0x14A), EEPROM.read(0x14B) );
 } else if (nid == 6) {
  tmp = ( EEPROM.read(0x150), EEPROM.read(0x151), EEPROM.read(0x152), EEPROM.read(0x153), 
          EEPROM.read(0x154), EEPROM.read(0x155), EEPROM.read(0x156), EEPROM.read(0x157), 
          EEPROM.read(0x158), EEPROM.read(0x159), EEPROM.read(0x15A), EEPROM.read(0x15B) );
 } else if (nid == 7) {
  tmp = ( EEPROM.read(0x160), EEPROM.read(0x161), EEPROM.read(0x162), EEPROM.read(0x163), 
          EEPROM.read(0x164), EEPROM.read(0x165), EEPROM.read(0x166), EEPROM.read(0x167), 
          EEPROM.read(0x168), EEPROM.read(0x169), EEPROM.read(0x16A), EEPROM.read(0x16B) );
 } else if (nid == 8) {
  tmp = ( EEPROM.read(0x170), EEPROM.read(0x171), EEPROM.read(0x172), EEPROM.read(0x173), 
          EEPROM.read(0x174), EEPROM.read(0x175), EEPROM.read(0x176), EEPROM.read(0x177), 
          EEPROM.read(0x178), EEPROM.read(0x179), EEPROM.read(0x17A), EEPROM.read(0x17B) ); 
 } else if (nid == 9) {
  tmp = ( EEPROM.read(0x180), EEPROM.read(0x181), EEPROM.read(0x182), EEPROM.read(0x183), 
          EEPROM.read(0x184), EEPROM.read(0x185), EEPROM.read(0x186), EEPROM.read(0x187), 
          EEPROM.read(0x188), EEPROM.read(0x189), EEPROM.read(0x18A), EEPROM.read(0x18B) ); 
 } else if (nid == 10) {
  tmp = ( EEPROM.read(0x190), EEPROM.read(0x191), EEPROM.read(0x192), EEPROM.read(0x193), 
          EEPROM.read(0x194), EEPROM.read(0x195), EEPROM.read(0x196), EEPROM.read(0x197), 
          EEPROM.read(0x198), EEPROM.read(0x199), EEPROM.read(0x19A), EEPROM.read(0x19B) ); 
 }
 return tmp;
}

void ProfSelected() {
 lcd.clear();
 lcd.setCursor(0,0);
 lcd.print("Profile Selected");
 delay(1500);
}




